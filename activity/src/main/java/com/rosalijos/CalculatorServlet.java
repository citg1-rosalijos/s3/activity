package com.rosalijos;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	private static final long serialVersionUID = 7409906329253230239L;

	public void init() throws ServletException {
		System.out.println("****************************************");
		System.out.println("Calculator Servlet has been initialized.");
		System.out.println("****************************************");
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<h1>You are now using the Rosalijos Calculator app.</h1>"+
					"To use the app, input two numbers and an operation.<br/><br/>"+
					"Hit the \"Get Results\" button after filling in the details<br/><br/>"+
					"You will get the result shown in your browser!<br/><br/>");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		PrintWriter out = response.getWriter();
		
		double num1 = Double.parseDouble(request.getParameter("num1"));
		double num2 = Double.parseDouble(request.getParameter("num2"));
		double result = 0.0;
		String operation = request.getParameter("operation");
		
		
		switch(operation) {
			case "add": 
				result = num1 + num2; 
				break;
			case "subtract": 
				result = num1 - num2; 
				break;
			case "multiply": 
				result = num1 * num2; 
				break;
			case "divide": 
				result = num1 / num2; 
				break;
		}
		
		out.println("<br/><br/>The two numbers you provided are: " + num1 + ", " + num2 +
					"<br/><br/>The operation you entered is: " + operation + 
					"<br/><br/>The result is: " + result);
		
	}
	
	public void destroy() {
		System.out.println("**************************************");
		System.out.println("Calculator Servlet has been destroyed.");
		System.out.println("**************************************");
	}
}
